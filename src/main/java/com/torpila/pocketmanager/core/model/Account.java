package com.torpila.pocketmanager.core.model;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "type_id", "user_id"})}
)
public class Account {

    @Expose
    private Long id;
    @Expose
    private String name;
    @Expose
    private Double budget;
    @Expose
    private Double notificationLimit;
    @Expose
    private Currency currency;
    @Expose
    private AccountType type;
    private User user;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public Double getNotificationLimit() {
        return notificationLimit;
    }

    public void setNotificationLimit(Double notificationLimit) {
        this.notificationLimit = notificationLimit;
    }

    @ManyToOne
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @ManyToOne
    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    @ManyToOne
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this);
    }
}
