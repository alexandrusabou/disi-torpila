package com.torpila.pocketmanager.core.service;

import com.torpila.pocketmanager.core.model.Account;
import com.torpila.pocketmanager.core.model.AccountType;
import com.torpila.pocketmanager.core.model.User;

import java.util.List;

public interface AccountService {

    Account addAccount(Account account);

    Account getByNameAndAccountTypeForCurrentUser(String name, AccountType accountType, String userEmail);

    void removeAccount(long idAccount);

    Account save(Account account);

    List<Account> getAll();

    Account getAccountById(Long id);

    List<Account> getNotificationAccounts(List<Account> accounts);


}
