package com.torpila.pocketmanager.core.service.impl;

import com.torpila.pocketmanager.core.model.Account;
import com.torpila.pocketmanager.core.model.AccountType;
import com.torpila.pocketmanager.core.repository.AccountTypeRepository;
import com.torpila.pocketmanager.core.service.AccountTypeService;
import com.torpila.pocketmanager.web.form.AccountTypeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountTypeServiceImpl implements AccountTypeService {

    private AccountTypeRepository accountTypeRepository;

    @Override
    public List<AccountType> getAll() {
        final List<AccountType> accountTypes = new ArrayList<>();
        getAccountTypeRepository().findAll().forEach(accountTypes::add);
        return accountTypes;
    }

    public AccountTypeRepository getAccountTypeRepository() {
        return accountTypeRepository;
    }

    @Autowired
    public void setAccountTypeRepository(AccountTypeRepository accountTypeRepository) {
        this.accountTypeRepository = accountTypeRepository;
    }

    @Override
    public AccountType save(AccountType accountType) {
        return accountTypeRepository.save(accountType);
    }



    @Override
    public void deleteAccountType(AccountType accountType) {
        accountTypeRepository.delete(accountType);
    }

    @Override
    public AccountType getByName(String name) {

        return accountTypeRepository.findByName(name).orElse(null);
    }
}
