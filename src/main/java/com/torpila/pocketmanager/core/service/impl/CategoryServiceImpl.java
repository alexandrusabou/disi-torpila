package com.torpila.pocketmanager.core.service.impl;

import com.torpila.pocketmanager.core.model.Category;
import com.torpila.pocketmanager.core.repository.CategoryRepository;
import com.torpila.pocketmanager.core.service.CategoryService;
import com.torpila.pocketmanager.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;
    private UserService userService;


    @Override
    public List<Category> getAll() {
        final List<Category> categories = new ArrayList<>();
        getCategoryRepository().findAllByUserEmail(getUserService().getCurrentUserEmail()).forEach(categories::add);
        return categories;
    }

    @Override
    public Category save(Category category) {
        if (Objects.isNull(category.getUser())) {
            category.setUser(getUserService().getCurrentUser());
        }
        return getCategoryRepository().save(category);
    }

    @Override
    public void deleteById(Long id) {
        getCategoryRepository().deleteById(id);
    }

    @Override
    public Category getByName(String name) {
        return getCategoryRepository().findByNameAndUserEmail(name,getUserService().getCurrentUserEmail())
                .orElse(null);
    }


    public CategoryRepository getCategoryRepository() {
        return categoryRepository;
    }

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    public UserService getUserService() {
        return userService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }



}
