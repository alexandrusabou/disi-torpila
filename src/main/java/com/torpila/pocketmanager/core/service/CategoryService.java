package com.torpila.pocketmanager.core.service;

import com.torpila.pocketmanager.core.model.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category save(Category category);

    void deleteById(Long id);

    Category getByName(String name);

}
