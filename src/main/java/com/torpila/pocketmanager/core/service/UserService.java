package com.torpila.pocketmanager.core.service;

import com.torpila.pocketmanager.core.model.Currency;
import com.torpila.pocketmanager.core.model.User;

public interface UserService {

    User login(final String email, final String password);

    User getByEmail(final String email);

    String getCurrentUserEmail();

    User getCurrentUser();

    void updateCurrencyUser(Currency currency);
}
