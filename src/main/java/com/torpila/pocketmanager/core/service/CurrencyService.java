package com.torpila.pocketmanager.core.service;

import com.torpila.pocketmanager.core.model.Currency;

import com.torpila.pocketmanager.web.form.CurrencyForm;
import java.util.List;

public interface CurrencyService {

    List<Currency> getAll();

    Currency save(Currency currency);

    void deleteById(Long id);

    Currency getByCode(String id);

}
