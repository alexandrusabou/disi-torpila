package com.torpila.pocketmanager.core.service.impl;

import com.torpila.pocketmanager.core.model.Account;
import com.torpila.pocketmanager.core.model.AccountType;
import com.torpila.pocketmanager.core.model.User;
import com.torpila.pocketmanager.core.repository.AccountRepository;
import com.torpila.pocketmanager.core.service.AccountService;
import com.torpila.pocketmanager.core.service.UserService;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;
    private UserService userService;

    /* Add */
    @Override
    public Account addAccount(Account account) {
        return getAccountRepository().save(account);
    }

    @Override
    public Account getByNameAndAccountTypeForCurrentUser(String name, AccountType accountType, String userEmail) {
        return getAccountRepository().findFirstByNameAndTypeAndUserEmail(name, accountType, userEmail);
    }

    @Override
    public void removeAccount(long idAccount) {
        accountRepository.deleteById(idAccount);
    }


    @Override
    public Account save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public List<Account> getAll() {
        return getAccountRepository().findAllByUserEmail(getUserService().getCurrentUserEmail());
    }

    @Override
    public Account getAccountById(Long id) {
        return accountRepository.findAccountById(id);
    }

    @Override
    public List<Account> getNotificationAccounts(List<Account> accounts) {
        if (accounts != null) {
            return accounts.stream().filter(account -> account.getNotificationLimit() != null && account.getBudget() <= account.getNotificationLimit())
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }



    public AccountRepository getAccountRepository() {
        return accountRepository;
    }


    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public UserService getUserService() {
        return userService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
