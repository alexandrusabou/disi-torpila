package com.torpila.pocketmanager.core.service;

import com.torpila.pocketmanager.core.model.AccountType;

import com.torpila.pocketmanager.web.form.AccountTypeForm;
import java.util.List;

public interface AccountTypeService {

    List<AccountType> getAll();

    AccountType save(AccountType accountType);


    void deleteAccountType(AccountType accountType);

    AccountType getByName(String name);
}
