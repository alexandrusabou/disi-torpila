package com.torpila.pocketmanager.core.service.impl;

import com.torpila.pocketmanager.core.model.Currency;
import com.torpila.pocketmanager.core.model.User;
import com.torpila.pocketmanager.core.repository.UserRepository;
import com.torpila.pocketmanager.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Override
    public User login(final String email, final String password) {
        return getUserRepository().findFirstByEmailAndPassword(email, password);
    }

    @Override
    public User getByEmail(final String email) {
        return getUserRepository().findByEmail(email);
    }

    @Override
    public String getCurrentUserEmail() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        } else {
            return principal.toString();
        }
    }

    @Override
    public User getCurrentUser() {
        return getByEmail(getCurrentUserEmail());
    }

    @Override
    public void updateCurrencyUser(Currency currency) {
        User currentUser = getCurrentUser();
        currentUser.setDefaultCurrency(currency);
        userRepository.save(currentUser);
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


}
