package com.torpila.pocketmanager.core.service.impl;

import com.torpila.pocketmanager.core.model.Currency;
import com.torpila.pocketmanager.core.repository.CurrencyRepository;
import com.torpila.pocketmanager.core.service.CurrencyService;
import com.torpila.pocketmanager.core.service.UserService;
import com.torpila.pocketmanager.web.form.CurrencyForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private UserService userService;
    private CurrencyRepository currencyRepository;

    @Override
    public List<Currency> getAll() {
        final List<Currency> currencies = new ArrayList<>();
        getCurrencyRepository().findAllByUserEmail(getUserService().getCurrentUserEmail()).forEach(currencies::add);
        return currencies;
    }

    @Override
    public Currency save(Currency currency) {
        if (Objects.isNull(currency.getUser())) {
            currency.setUser(getUserService().getCurrentUser());
        }
        return getCurrencyRepository().save(currency);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        getCurrencyRepository().deleteById(id);
    }

    @Override
    public Currency getByCode(String code) {
        return getCurrencyRepository().findByCodeAndUserEmail(code, getUserService().getCurrentUserEmail())
                .orElse(null);
    }

    public CurrencyRepository getCurrencyRepository() {
        return currencyRepository;
    }

    @Autowired
    public void setCurrencyRepository(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    public UserService getUserService() {
        return userService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


}
