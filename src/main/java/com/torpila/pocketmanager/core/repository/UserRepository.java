package com.torpila.pocketmanager.core.repository;

import com.torpila.pocketmanager.core.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findFirstByEmailAndPassword(final String email, final String password);
    User findByEmail(final String email);
}
