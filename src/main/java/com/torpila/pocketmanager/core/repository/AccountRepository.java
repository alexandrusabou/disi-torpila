package com.torpila.pocketmanager.core.repository;

import com.torpila.pocketmanager.core.model.Account;
import com.torpila.pocketmanager.core.model.AccountType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepository extends CrudRepository<Account, Long> {

    Account findFirstByNameAndTypeAndUserEmail(final String name, final AccountType accountType, final String userEmail);

    Account findAccountById(Long id);

    List<Account> findAllByUserEmail(final String email);
}
