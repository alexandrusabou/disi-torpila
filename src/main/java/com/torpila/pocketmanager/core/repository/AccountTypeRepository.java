package com.torpila.pocketmanager.core.repository;

import com.torpila.pocketmanager.core.model.AccountType;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface AccountTypeRepository extends CrudRepository<AccountType, Long> {

  Optional<AccountType> findByName(String name);

}
