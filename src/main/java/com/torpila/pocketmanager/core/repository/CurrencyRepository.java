package com.torpila.pocketmanager.core.repository;

import com.torpila.pocketmanager.core.model.Currency;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CurrencyRepository extends CrudRepository<Currency, Long> {
    List<Currency> findAllByUserEmail(String email);
    Optional<Currency> findByCodeAndUserEmail(String code, String email);
}
