package com.torpila.pocketmanager.core.repository;

import com.torpila.pocketmanager.core.model.Category;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    List<Category> findAllByUserEmail(String email);

    Optional<Category> findByNameAndUserEmail(String name, String email);

}
