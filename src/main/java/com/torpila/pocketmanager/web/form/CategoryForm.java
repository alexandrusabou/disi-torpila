package com.torpila.pocketmanager.web.form;

import com.torpila.pocketmanager.core.model.Category;

public class CategoryForm {

    private Category category;
    private String editName;
    private String addName;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getEditName() {
        return editName;
    }

    public void setEditName(String editName) {
        this.editName = editName;
    }

    public String getAddName() {
        return addName;
    }

    public void setAddName(String addName) {
        this.addName = addName;
    }

}
