package com.torpila.pocketmanager.web.form;

import com.torpila.pocketmanager.core.model.Currency;

public class DefaultCurrencyForm {

    private Currency defaultCurrency;
    private Currency currency;// dupa ce e setat, sa fie diferit de default currency

    public DefaultCurrencyForm() {
    }

    public DefaultCurrencyForm(Currency defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public Currency getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(Currency defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
