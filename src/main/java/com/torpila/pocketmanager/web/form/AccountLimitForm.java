package com.torpila.pocketmanager.web.form;

import com.torpila.pocketmanager.core.model.Account;

public class AccountLimitForm {

    private Account account;
    private Double limit;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Double getLimit() {
        return limit;
    }

    public void setLimit(Double limit) {
        this.limit = limit;
    }
}
