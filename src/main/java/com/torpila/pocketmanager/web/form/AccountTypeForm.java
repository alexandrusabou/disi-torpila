package com.torpila.pocketmanager.web.form;

import com.torpila.pocketmanager.core.model.AccountType;

public class AccountTypeForm {

    private AccountType accountType;
    private String editName;
    private String addName;

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getEditName() {
        return editName;
    }

    public void setEditName(String editName) {
        this.editName = editName;
    }

    public String getAddName() {
        return addName;
    }

    public void setAddName(String addName) {
        this.addName = addName;
    }
}
