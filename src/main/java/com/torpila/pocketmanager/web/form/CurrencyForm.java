package com.torpila.pocketmanager.web.form;

import com.torpila.pocketmanager.core.model.Currency;

public class CurrencyForm {
    private Currency currency;
    private String code;
    private String editName;
    private String addName;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEditName() {
        return editName;
    }

    public void setEditName(String editName) {
        this.editName = editName;
    }

    public String getAddName() {
        return addName;
    }

    public void setAddName(String addName) {
        this.addName = addName;
    }
}
