package com.torpila.pocketmanager.web.controller;

import com.torpila.pocketmanager.core.model.Category;
import com.torpila.pocketmanager.core.service.CategoryService;
import com.torpila.pocketmanager.core.service.UserService;
import com.torpila.pocketmanager.web.form.CategoryForm;
import com.torpila.pocketmanager.web.validator.CategoryFormValidator;
import com.torpila.pocketmanager.web.validator.EditCategoryFormValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Objects;

@Controller
@RequestMapping(value = "/category")
public class CategoryController {

    private final CategoryService categoryService;
    private final CategoryFormValidator categoryFormValidator;
    private final EditCategoryFormValidator editCategoryFormValidator;

    public CategoryController(CategoryService categoryService,
        CategoryFormValidator categoryFormValidator,
        EditCategoryFormValidator editCategoryFormValidator) {
        this.categoryService = categoryService;
        this.categoryFormValidator = categoryFormValidator;
        this.editCategoryFormValidator = editCategoryFormValidator;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editCategory(final CategoryForm categoryForm, final RedirectAttributes redirectAttributes,
        final BindingResult bindingResult, final Model model) {

        editCategoryFormValidator.validate(categoryForm, bindingResult);
        if (bindingResult.hasErrors()) {
            copyModelToRedirect(model, redirectAttributes);
            redirectAttributes.addFlashAttribute("errorMessage", "Oops, some errors have occurred");
            return "redirect:/settings";
        }

        if (categoryForm.getCategory().getId() != null)
        {
            Category category=new Category();

            category.setId(categoryForm.getCategory().getId());
            category.setName(categoryForm.getEditName());

            final Category newCategory = categoryService.save(category);
            if (Objects.nonNull(newCategory)) {
                redirectAttributes.addFlashAttribute("successMessage", "The category has been edited");
            }
        }

        return "redirect:/settings";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addCategory(final CategoryForm categoryForm, final RedirectAttributes redirectAttributes,
                              final BindingResult bindingResult, final Model model) {
        categoryFormValidator.validate(categoryForm, bindingResult);
        if (bindingResult.hasErrors()) {
            copyModelToRedirect(model, redirectAttributes);
            redirectAttributes.addFlashAttribute("errorMessage", "Oops, some errors have occurred");
            return "redirect:/settings";
        }

        Category category = new Category();
        category.setName(categoryForm.getAddName());

        final Category newCategory = categoryService.save(category);
        if (Objects.nonNull(newCategory)) {
            redirectAttributes.addFlashAttribute("successMessage", "The category has been created");
        }
        return "redirect:/settings";

    }


    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteCategory(final CategoryForm categoryForm,final RedirectAttributes redirectAttributes) {

        if(Objects.nonNull(categoryForm.getCategory())) {
            categoryService.deleteById(categoryForm.getCategory().getId());
            redirectAttributes.addFlashAttribute("successMessage", "The category has been deleted");
        }
        return "redirect:/settings";
    }

    private void copyModelToRedirect(final Model model, final RedirectAttributes redirectAttributes) {
        model.asMap().forEach(redirectAttributes::addFlashAttribute);
    }
}
