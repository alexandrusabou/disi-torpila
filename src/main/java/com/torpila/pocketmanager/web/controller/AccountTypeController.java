package com.torpila.pocketmanager.web.controller;

import com.torpila.pocketmanager.core.model.AccountType;
import com.torpila.pocketmanager.core.service.AccountTypeService;
import com.torpila.pocketmanager.web.form.AccountTypeForm;
import com.torpila.pocketmanager.web.validator.AccountTypeFormValidator;
import com.torpila.pocketmanager.web.validator.EditAccountTypeFormValidator;
import java.util.Objects;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/accountType")
public class AccountTypeController {


    private final AccountTypeService accountTypeService;
    private final AccountTypeFormValidator accountTypeValidator;
    private final EditAccountTypeFormValidator editAccountTypeValidator;

    public AccountTypeController(
        AccountTypeService accountTypeService,
        AccountTypeFormValidator accountTypeValidator,
        EditAccountTypeFormValidator editAccountTypeValidator) {
        this.accountTypeService = accountTypeService;
        this.accountTypeValidator = accountTypeValidator;
        this.editAccountTypeValidator = editAccountTypeValidator;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editCurrency(final AccountTypeForm accountTypeForm, final RedirectAttributes redirectAttributes,
        final BindingResult bindingResult, final Model model) {

        editAccountTypeValidator.validate(accountTypeForm, bindingResult);
        if (bindingResult.hasErrors()) {
            copyModelToRedirect(model, redirectAttributes);
            redirectAttributes.addFlashAttribute("errorMessage", "Oops, some errors have occurred");
            return "redirect:/settings";
        }

        if(accountTypeForm.getAccountType().getId()!=null)
        {
            AccountType accountType=new AccountType();
            accountType.setId(accountTypeForm.getAccountType().getId());
            accountType.setName(accountTypeForm.getEditName());
            accountType.setAccounts(accountTypeForm.getAccountType().getAccounts());

            final AccountType newAccountType = accountTypeService.save(accountType);
            if(Objects.nonNull(newAccountType))
            {
                redirectAttributes.addFlashAttribute("successMessage", "The account type has been edited");
            }
        }


        return "redirect:/settings";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addCurrency(final AccountTypeForm accountTypeForm, final RedirectAttributes redirectAttributes,
        final BindingResult bindingResult, final Model model) {

        accountTypeValidator.validate(accountTypeForm, bindingResult);
        if (bindingResult.hasErrors()) {
            copyModelToRedirect(model, redirectAttributes);
            redirectAttributes.addFlashAttribute("errorMessage", "Oops, some errors have occurred");
            return "redirect:/settings";
        }

        AccountType accountType=new AccountType();
        accountType.setName(accountTypeForm.getAddName());

        final AccountType newAccountType = accountTypeService.save(accountType);
        if(Objects.nonNull(newAccountType))
        {
            redirectAttributes.addFlashAttribute("successMessage", "The account type has been created");
        }



        return "redirect:/settings";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteCurrency(final AccountTypeForm accountTypeForm, final RedirectAttributes redirectAttributes,
        final BindingResult bindingResult, final Model model) {

        if(!CollectionUtils.isEmpty(accountTypeForm.getAccountType().getAccounts()))
        {
            redirectAttributes.addFlashAttribute("errorMessage", "There are accounts that use this account type");
            return "redirect:/settings";
        }

        if (Objects.nonNull(accountTypeForm.getAccountType())){
            accountTypeService.deleteAccountType(accountTypeForm.getAccountType());
            redirectAttributes.addFlashAttribute("successMessage", "The account type has been deleted");
        }


        return "redirect:/settings";
    }

    private void copyModelToRedirect(final Model model, final RedirectAttributes redirectAttributes) {
        model.asMap().forEach(redirectAttributes::addFlashAttribute);
    }
}
