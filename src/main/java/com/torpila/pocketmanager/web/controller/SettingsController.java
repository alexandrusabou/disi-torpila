package com.torpila.pocketmanager.web.controller;

import com.mysql.cj.xdevapi.Collection;
import com.torpila.pocketmanager.core.model.Account;
import com.torpila.pocketmanager.core.model.AccountType;
import com.torpila.pocketmanager.core.model.Currency;
import com.torpila.pocketmanager.core.service.*;
import com.torpila.pocketmanager.web.form.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;

@Controller
@RequestMapping(value = "/settings")
public class SettingsController {

    private final CurrencyService currencyService;

    @Autowired
    private AccountTypeService accountTypeService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UserService userService;

    @Autowired
    private AccountService  accountService;

    public SettingsController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showSettingsPage(final Model model) {
        model.addAttribute("currencies", currencyService.getAll());
        model.addAttribute("accountTypes",accountTypeService.getAll());
        model.addAttribute("categories", categoryService.getAll());
        Currency defaultCurrency = !CollectionUtils.isEmpty(currencyService.getAll()) ? currencyService.getAll().get(0) : null;

        //NU STERGETI liniile urmatoare, puteti modifica obiectele daca vreti
        if (!model.containsAttribute("currencyForm")) {
            model.addAttribute("currencyForm", new CurrencyForm());
        }
        if (!model.containsAttribute("categoryForm")) {
            model.addAttribute("categoryForm", new CategoryForm());
        }
        if (!model.containsAttribute("accountTypeForm")) {
            model.addAttribute("accountTypeForm", new AccountTypeForm());
        }
        if (!model.containsAttribute("accountLimitForm")) {
            model.addAttribute("accountLimitForm", new AccountLimitForm());
        }

        if(userService.getCurrentUser().getDefaultCurrency()!=null)
        {
            model.addAttribute("defaultCurrencyForm", new DefaultCurrencyForm(userService.getCurrentUser().getDefaultCurrency()));
        }
        else
        {
            model.addAttribute("defaultCurrencyForm", new DefaultCurrencyForm(defaultCurrency));
        }

         model.addAttribute("accounts",accountService.getAll());



        return "settings/settingsPage";
    }
}
