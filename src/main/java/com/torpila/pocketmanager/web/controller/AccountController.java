package com.torpila.pocketmanager.web.controller;

import com.torpila.pocketmanager.core.model.Account;
import com.torpila.pocketmanager.core.model.User;
import com.torpila.pocketmanager.core.service.AccountService;
import com.torpila.pocketmanager.core.service.AccountTypeService;
import com.torpila.pocketmanager.core.service.CurrencyService;
import com.torpila.pocketmanager.core.service.UserService;
import com.torpila.pocketmanager.web.form.AccountForm;
import com.torpila.pocketmanager.web.form.AccountLimitForm;
import com.torpila.pocketmanager.web.validator.AccountFormValidator;
import com.torpila.pocketmanager.web.validator.AccountLimitFormValidator;
import com.torpila.pocketmanager.web.validator.EditAccountFormValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/account")
public class AccountController {

    private final CurrencyService currencyService;
    private final AccountTypeService accountTypeService;
    private final AccountFormValidator accountFormValidator;
    private final EditAccountFormValidator editAccountFormValidator;
    private final AccountService accountService;
    private final UserService userService;
    private final AccountLimitFormValidator accountLimitFormValidator;

    public AccountController(CurrencyService currencyService, AccountTypeService accountTypeService, AccountFormValidator accountFormValidator, EditAccountFormValidator editAccountFormValidator, AccountService accountService, UserService userService, AccountLimitFormValidator accountLimitFormValidator) {
        this.currencyService = currencyService;
        this.accountTypeService = accountTypeService;
        this.accountFormValidator = accountFormValidator;
        this.editAccountFormValidator = editAccountFormValidator;
        this.accountService = accountService;
        this.userService = userService;
        this.accountLimitFormValidator = accountLimitFormValidator;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String showAddAccountPage(final Model model) {
        final AccountForm accountForm = new AccountForm();
        final User currentUser = userService.getCurrentUser();

        if (currentUser != null && currentUser.getDefaultCurrency() != null) {
            accountForm.setCurrency(currentUser.getDefaultCurrency());
        }

        populateModelForAddAccount(model, accountForm);

        return "account/addAccountPage";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addAccount(final AccountForm accountForm, final BindingResult bindingResult, final Model model, final Principal principal) {
        accountFormValidator.validate(accountForm, bindingResult);
        if (bindingResult.hasErrors()) {
            populateModelForAddAccount(model, accountForm);
            return "account/addAccountPage";
        }
        final User currentUser = userService.getByEmail(principal.getName());
        final Account account = new Account();
        account.setName(accountForm.getName());
        account.setBudget(accountForm.getBudget());
        account.setCurrency(accountForm.getCurrency());
        account.setType(accountForm.getAccountType());
        account.setUser(currentUser);

        accountService.addAccount(account);

        return "redirect:/";
    }



    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String showEditAccountPage(@PathVariable("id") Long id ,final Model model){

        Account account = accountService.getAccountById(id);
        if(account != null){
            AccountForm accountForm=new AccountForm();
            accountForm.setId(account.getId());
            accountForm.setName(account.getName());
            accountForm.setAccountType(account.getType());
            accountForm.setBudget(account.getBudget());
            accountForm.setCurrency(account.getCurrency());
            populateModelForAddAccount(model, accountForm );

        }
        return "account/editAccountPage";
    }

    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public String editAccount(final AccountForm accountForm, final BindingResult bindingResult, final Model model, final Principal principal){
        editAccountFormValidator.validate(accountForm, bindingResult);
        if(bindingResult.hasErrors()){
            populateModelForAddAccount(model, accountForm);
            return "account/editAccountPage";
        }
        if (accountForm.getId() != null) {
            Account account = accountService.getAccountById(accountForm.getId());

            account.setName(accountForm.getName());
            account.setBudget(accountForm.getBudget());
            account.setCurrency(accountForm.getCurrency());
            account.setType(accountForm.getAccountType());

            accountService.save(account);
        } else {
            return "redirect:/";
        }


        return "redirect:/";
    }



    @RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
    public String removeAccount(@PathVariable("id") Long idAccount) {
        accountService.removeAccount(idAccount);

        return "redirect:/";
    }

    @PostMapping("/edit/{id}")
    public Account editAccount(@PathVariable("id") long id, @Valid Account account) {
        account.setId(id);
        return accountService.save(account);

    }

    @RequestMapping(value = "/limit/update", method = RequestMethod.POST)
    public String updateAccountLimit(final AccountLimitForm accountLimitForm,final RedirectAttributes redirectAttributes,
                                     final BindingResult bindingResult, final Model model) {

        accountLimitFormValidator.validate(accountLimitForm,bindingResult);
        if (bindingResult.hasErrors()) {
            copyModelToRedirect(model, redirectAttributes);
            redirectAttributes.addFlashAttribute("errorMessage", "Oops, some errors have occurred");
            return "redirect:/settings";
        }

        if(accountLimitForm.getAccount().getId()!=null) {
            Account account = accountService.getAccountById(accountLimitForm.getAccount().getId());
            account.setNotificationLimit(accountLimitForm.getLimit());
            accountService.save(account);
            redirectAttributes.addFlashAttribute("successMessage", "The limit has been changed");
        }
        return "redirect:/settings";
    }

    private void populateModelForAddAccount(final Model model, final AccountForm accountForm) {
        model.addAttribute("accountForm", accountForm);
        model.addAttribute("currencies", currencyService.getAll());
        model.addAttribute("accountTypes", accountTypeService.getAll());
    }

    private void copyModelToRedirect(final Model model, final RedirectAttributes redirectAttributes) {
        model.asMap().forEach(redirectAttributes::addFlashAttribute);
    }
}
