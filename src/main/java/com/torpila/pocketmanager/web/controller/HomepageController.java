package com.torpila.pocketmanager.web.controller;

import com.torpila.pocketmanager.core.model.Account;
import com.torpila.pocketmanager.core.service.AccountService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping(value = "/")
public class HomepageController {

    private final AccountService accountService;

    public HomepageController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showAll(final Model model) {
        final List<Account> accounts = accountService.getAll();

        model.addAttribute("accounts", accounts);
        model.addAttribute("notificationAccounts", accountService.getNotificationAccounts(accounts));
        return "index";
    }
}
