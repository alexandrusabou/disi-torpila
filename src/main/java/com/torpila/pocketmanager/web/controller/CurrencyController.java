package com.torpila.pocketmanager.web.controller;

import com.torpila.pocketmanager.core.model.Currency;
import com.torpila.pocketmanager.core.service.CurrencyService;
import com.torpila.pocketmanager.core.service.UserService;
import com.torpila.pocketmanager.web.form.CurrencyForm;
import com.torpila.pocketmanager.web.validator.CurrencyFormValidator;
import com.torpila.pocketmanager.web.validator.EditCurrencyFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping(value = "/currency")
public class CurrencyController {

    private final UserService userService;
    private final CurrencyService currencyService;
    private final CurrencyFormValidator currencyFormValidator;
    private final EditCurrencyFormValidator editCurrencyFormValidator;

    public CurrencyController(UserService userService,
        CurrencyService currencyService,
        CurrencyFormValidator currencyFormValidator,
        EditCurrencyFormValidator editCurrencyFormValidator) {
        this.userService = userService;
        this.currencyService = currencyService;
        this.currencyFormValidator = currencyFormValidator;
        this.editCurrencyFormValidator = editCurrencyFormValidator;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editCurrency(final CurrencyForm currencyForm, final RedirectAttributes redirectAttributes,
                                final BindingResult bindingResult, final Model model) {

        editCurrencyFormValidator.validate(currencyForm, bindingResult);
        if (bindingResult.hasErrors()) {
            copyModelToRedirect(model, redirectAttributes);
            redirectAttributes.addFlashAttribute("errorMessage", "Oops, some errors have occurred");
            return "redirect:/settings";
        }

        if(currencyForm.getCurrency().getId()!=null)
        {
            Currency currency=new Currency();

            currency.setId(currencyForm.getCurrency().getId());
            currency.setName(currencyForm.getEditName());
            currency.setCode(currencyForm.getCurrency().getCode());
            currency.setUser(currencyForm.getCurrency().getUser());
            currency.setAccounts(currencyForm.getCurrency().getAccounts());

            final Currency newCurrency = currencyService.save(currency);
            if(Objects.nonNull(newCurrency))
            {
                redirectAttributes.addFlashAttribute("successMessage", "The currency has been edited");
            }
        }

        return "redirect:/settings";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addCurrency(final CurrencyForm currencyForm, final RedirectAttributes redirectAttributes,
                              final BindingResult bindingResult, final Model model) {
        currencyFormValidator.validate(currencyForm, bindingResult);
        if (bindingResult.hasErrors()) {
            copyModelToRedirect(model, redirectAttributes);
            redirectAttributes.addFlashAttribute("errorMessage", "Oops, some errors have occurred");
            return "redirect:/settings";
        }

        final Currency newCurrency = currencyService.save(buildCurrency(currencyForm));
        if (Objects.nonNull(newCurrency)) {
            redirectAttributes.addFlashAttribute("successMessage", "The currency has been created");
        }

        return "redirect:/settings";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteCurrency(final CurrencyForm currencyForm, final RedirectAttributes redirectAttributes) {
        if (!CollectionUtils.isEmpty(currencyForm.getCurrency().getAccounts())) {
            redirectAttributes.addFlashAttribute("errorMessage", "There are accounts that use this currency");
            return "redirect:/settings";
        }

        if (Objects.nonNull(currencyForm.getCurrency())) {
            currencyService.deleteById(currencyForm.getCurrency().getId());
            redirectAttributes.addFlashAttribute("successMessage", "The currency has been deleted");
        }

        return "redirect:/settings";
    }

    private Currency buildCurrency(final CurrencyForm currencyForm) {
        final Currency currency = new Currency();

        currency.setCode(currencyForm.getCode());
        currency.setName(currencyForm.getAddName());

        return currency;
    }

    private void copyModelToRedirect(final Model model, final RedirectAttributes redirectAttributes) {
        model.asMap().forEach(redirectAttributes::addFlashAttribute);
    }
}
