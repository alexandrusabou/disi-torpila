package com.torpila.pocketmanager.web.controller;

import com.torpila.pocketmanager.core.model.User;
import com.torpila.pocketmanager.core.service.UserService;
import com.torpila.pocketmanager.core.service.impl.UserServiceImpl;
import com.torpila.pocketmanager.web.form.DefaultCurrencyForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/user")
public class UserController {


    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/currency/update", method = RequestMethod.POST)
    public String updateDefaultCurrency(final DefaultCurrencyForm defaultCurrencyForm, final RedirectAttributes redirectAttributes) {

    if(!defaultCurrencyForm.getCurrency().equals(defaultCurrencyForm.getDefaultCurrency()))
        {
            userService.updateCurrencyUser(defaultCurrencyForm.getCurrency());
            redirectAttributes.addFlashAttribute("successMessage", "The default currency has been changed");
            System.out.println(defaultCurrencyForm.getCurrency());
        }


        return "redirect:/settings";
    }
}
