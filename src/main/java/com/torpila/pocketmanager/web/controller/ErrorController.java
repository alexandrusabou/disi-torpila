package com.torpila.pocketmanager.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController {

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String handleError(final HttpServletRequest request) {

        return "errorPage";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
