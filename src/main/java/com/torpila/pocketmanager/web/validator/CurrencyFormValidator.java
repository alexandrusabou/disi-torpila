package com.torpila.pocketmanager.web.validator;

import com.torpila.pocketmanager.core.model.Currency;
import com.torpila.pocketmanager.core.service.CurrencyService;
import com.torpila.pocketmanager.web.form.CurrencyForm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class CurrencyFormValidator implements Validator {

    private CurrencyService currencyService;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(CurrencyForm.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        final CurrencyForm currencyForm = (CurrencyForm) o;

        if (StringUtils.isBlank(currencyForm.getCode())) {
            errors.rejectValue("code", "", "The code must not be empty");
        }
        if (StringUtils.isBlank(currencyForm.getAddName())) {
            errors.rejectValue("addName", "", "The name must not be empty");
        }

        final Currency currency = getCurrencyService().getByCode(currencyForm.getCode());
        if (Objects.nonNull(currency)) {
            errors.rejectValue("code", "", "The currency with this code already exists");
        }
    }

    public CurrencyService getCurrencyService() {
        return currencyService;
    }

    @Autowired
    public void setCurrencyService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }
}
