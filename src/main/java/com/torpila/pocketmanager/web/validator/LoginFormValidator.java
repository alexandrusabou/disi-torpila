package com.torpila.pocketmanager.web.validator;

import com.torpila.pocketmanager.web.form.LoginForm;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class LoginFormValidator implements Validator {

    private static final int PASSWORD_MIN_LENGTH = 8;

    @Override
    public boolean supports(final Class<?> aClass) {
        return aClass.equals(LoginForm.class);
    }

    @Override
    public void validate(final Object object, final Errors errors) {
        final LoginForm loginForm = (LoginForm) object;

        if (StringUtils.isEmpty(loginForm.getEmail())) {
            errors.rejectValue("email", "", "Email must not be empty");
        }

        if (StringUtils.isEmpty(loginForm.getPassword()) || loginForm.getPassword().length() < 8) {
            errors.rejectValue("password", "", "Password must have at least " + PASSWORD_MIN_LENGTH + " characters.");
        }
    }
}
