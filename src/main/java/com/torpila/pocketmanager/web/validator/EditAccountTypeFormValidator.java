package com.torpila.pocketmanager.web.validator;

import com.torpila.pocketmanager.core.model.AccountType;
import com.torpila.pocketmanager.core.service.AccountTypeService;
import com.torpila.pocketmanager.web.form.AccountTypeForm;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class EditAccountTypeFormValidator implements Validator {

  private AccountTypeService accountTypeService;

  @Override
  public boolean supports(Class<?> aClass) {
    return aClass.equals(AccountTypeForm.class);
  }

  @Override
  public void validate(Object o, Errors errors) {
    final AccountTypeForm accountTypeForm = (AccountTypeForm) o;


    if(accountTypeForm.getAccountType().getName().equals(accountTypeForm.getEditName()))
      errors.rejectValue("editName","","The new name must not be the same with old name");

    final AccountType accountType = getAccountTypeService().getByName(accountTypeForm.getEditName());
    if (Objects.nonNull(accountType)) {
      errors.rejectValue("editName", "", "The account type already exists");
    }
  }


  public AccountTypeService getAccountTypeService() {
    return accountTypeService;
  }

  @Autowired
  public void setAccountTypeService(
      AccountTypeService accountTypeService) {
    this.accountTypeService = accountTypeService;
  }
}
