package com.torpila.pocketmanager.web.validator;

import com.torpila.pocketmanager.core.model.Account;
import com.torpila.pocketmanager.core.service.AccountService;
import com.torpila.pocketmanager.web.form.AccountForm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class AccountFormValidator implements Validator {

    private AccountService accountService;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(AccountForm.class);
    }

    @Override
    public void validate(Object object, Errors errors) {
        final AccountForm accountForm = (AccountForm) object;

        if (StringUtils.isBlank(accountForm.getName())) {
            errors.rejectValue("name", "", "Account name is required");
        }
        if (accountForm.getBudget() == null || accountForm.getBudget() < 0.0d) {
            errors.rejectValue("budget", "", "The budget has to be a positive number");
        }
        final String userEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        final Account account = getAccountService().getByNameAndAccountTypeForCurrentUser(accountForm.getName(), accountForm.getAccountType(), userEmail);
        if (account != null) {
            if (StringUtils.equals(account.getName(), accountForm.getName())) {
                errors.rejectValue("name", "", "Another account with the same name exists");
            }
            if (account.getType().getId().equals(accountForm.getAccountType().getId())) {
                errors.rejectValue("accountType", "", "Another account with the same accountType exists");
            }
        }
    }

    public AccountService getAccountService() {
        return accountService;
    }

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }
}
