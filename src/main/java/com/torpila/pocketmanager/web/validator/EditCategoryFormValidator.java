package com.torpila.pocketmanager.web.validator;

import com.torpila.pocketmanager.core.model.AccountType;
import com.torpila.pocketmanager.core.model.Category;
import com.torpila.pocketmanager.core.service.AccountTypeService;
import com.torpila.pocketmanager.core.service.CategoryService;
import com.torpila.pocketmanager.web.form.AccountTypeForm;
import com.torpila.pocketmanager.web.form.CategoryForm;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class EditCategoryFormValidator implements Validator {


  private CategoryService categoryService;

  @Override
  public boolean supports(Class<?> aClass) {
    return aClass.equals(CategoryForm.class);
  }

  @Override
  public void validate(Object o, Errors errors) {
    final CategoryForm categoryForm = (CategoryForm) o;

    if (StringUtils.isBlank(categoryForm.getEditName())) {
      errors.rejectValue("editName", "", "The name must not be empty");
    }
    final Category category = getCategoryService().getByName(categoryForm.getEditName());
    if (Objects.nonNull(category)) {
      errors.rejectValue("editName", "", "The category with this name already exists");

    }
  }

  public CategoryService getCategoryService() {
    return categoryService;
  }

  @Autowired
  public void setCategoryService(CategoryService categoryService) {
    this.categoryService = categoryService;
  }



}
