package com.torpila.pocketmanager.web.validator;

import com.torpila.pocketmanager.core.model.Account;
import com.torpila.pocketmanager.core.service.AccountService;
import com.torpila.pocketmanager.web.form.AccountForm;
import com.torpila.pocketmanager.web.form.AccountLimitForm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class AccountLimitFormValidator implements Validator{



    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(AccountForm.class);
    }

    @Override
    public void validate(Object object, Errors errors) {
        final AccountLimitForm accountLimitForm = (AccountLimitForm) object;

        if (accountLimitForm.getLimit() == null || accountLimitForm.getLimit() < 0.0d) {
            errors.rejectValue("limit", "", "The limit has to be a positive number");
        }

    }



}
