package com.torpila.pocketmanager.web.validator;

import com.torpila.pocketmanager.core.model.Currency;
import com.torpila.pocketmanager.core.service.CurrencyService;
import com.torpila.pocketmanager.web.form.CurrencyForm;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class EditCurrencyFormValidator implements Validator {

  private CurrencyService currencyService;

  @Override
  public boolean supports(Class<?> aClass) {
    return aClass.equals(CurrencyForm.class);
  }

  @Override
  public void validate(Object o, Errors errors) {
    final CurrencyForm currencyForm = (CurrencyForm) o;


    if (StringUtils.isBlank(currencyForm.getEditName())) {
      errors.rejectValue("editName", "", "The name must not be empty");
    }

    if(currencyForm.getCurrency().getName().equals(currencyForm.getEditName()))
      errors.rejectValue("editName","","The new name must not be the same with old name");

  }

  public CurrencyService getCurrencyService() {
    return currencyService;
  }

  @Autowired
  public void setCurrencyService(CurrencyService currencyService) {
    this.currencyService = currencyService;
  }

}
