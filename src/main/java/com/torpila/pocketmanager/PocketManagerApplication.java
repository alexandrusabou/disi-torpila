package com.torpila.pocketmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.torpila.pocketmanager")
public class PocketManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocketManagerApplication.class, args);
    }

}
