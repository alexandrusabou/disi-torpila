Settings = {
    bindCurrencyChange: function () {
        $('.js-currency-form').on('change',  'select', function () {
            var selectedIndex = $(this).prop('selectedIndex');
            var itemForm = $(this).closest('form');
            var values = itemForm.data('values');
            if (selectedIndex >= 0 && values) {
                var currentValue = values[selectedIndex].name;
                if (currentValue) {
                    itemForm.find('#editName').val(currentValue);
                }
            }
        });
    },

    bindAccountTypeChange: function () {
        $('.js-accountType-form').on('change',  'select', function () {
            var selectedIndex = $(this).prop('selectedIndex');
            var itemForm = $(this).closest('form');
            var values = itemForm.data('values');
            if (selectedIndex >= 0 && values) {
                var currentValue = values[selectedIndex].name;
                if (currentValue) {
                    itemForm.find('#editName').val(currentValue);
                }
            }
        });
    },

    bindCategoryChange: function () {
        $('.js-category-form').on('change',  'select', function () {
            var selectedIndex = $(this).prop('selectedIndex');
            var itemForm = $(this).closest('form');
            var values = itemForm.data('values');
            if (selectedIndex >= 0 && values) {
                var currentValue = values[selectedIndex].name;
                if (currentValue) {
                    itemForm.find('#editName').val(currentValue);
                }
            }
        });
    },

    bindAccountLimitChange: function () {
        $('.js-accountLimit-form').on('change',  'select', function () {
            var selectedIndex = $(this).prop('selectedIndex');
            var itemForm = $(this).closest('form');
            var values = itemForm.data('values');
            if (selectedIndex >= 0 && values) {
                var currentValue = values[selectedIndex].notificationLimit;
                if (currentValue >= 0) {
                    itemForm.find('#limit').val(currentValue);
                }
            }
        });
    },

    bindDefaultCurrencyRefresh: function () {
        $('.js-refresh-def-currency').on('click', function () {
            var itemForm = $(this).closest('form');
            var values = itemForm.data('values');
            var defaultValue = itemForm.data('default');
            if (defaultValue && values) {
                var defaultValueId = defaultValue.id.toString();
                var currentValue = itemForm.find('select option:selected');
                if (currentValue.val() !== defaultValueId) {
                    currentValue.prop('selected', false);
                    itemForm.find('select option').each(function () {
                        if ($(this).val() === defaultValueId) {
                            $(this).prop('selected', true);
                        }
                    });
                }
            }
        });
    },
    
    bindCancelDelete: function () {
        $('.js-cancel-delete').on('click', function () {
           $(this).closest('.collapse').collapse('hide');
        });
    },

    bindInfoTimeout: function () {
        setTimeout(function () {
            $('.js-timeout').fadeOut('slow');
        }, 5000);
    }
};

var Notification = {
    showNotificationAccountsModal: function () {
        var notificationModal = $('#notificationModel');
        var accounts = notificationModal.data('accounts');
        if (accounts && accounts.length > 0) {
            notificationModal.modal('show');
        }
    }
};

$(document).ready(function () {
    Settings.bindCurrencyChange();
    Settings.bindAccountTypeChange();
    Settings.bindCategoryChange();
    Settings.bindAccountLimitChange();
    Settings.bindDefaultCurrencyRefresh();
    Settings.bindCancelDelete();
    Settings.bindInfoTimeout();
    Notification.showNotificationAccountsModal();
});