<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/template" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="notifications" tagdir="/WEB-INF/tags/notifications" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<template:page pageTitle="Pocket Manager">
    <div class="container">
        <div class="content-container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12 page-title">
                    <h4 class="page-title-content mb-1">All accounts</h4>
                </div>
                <div class="col-md-4 col-xs-5">
                    <c:url var="addAccountUrl" value="/account/add"/>
                    <a href="${addAccountUrl}" class="btn btn-primary float-right">Add account</a>
                </div>
            </div>
            <div class="border-row-gray mb-4"></div>
            <div class="row px-3">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Type</th>
                            <th scope="col">Budget</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="account" items="${accounts}">
                            <c:url var="editAccountUrl" value="/account/edit/${account.id}"/>
                            <c:url var="deleteAccountUrl" value="/account/remove/${account.id}"/>
                            <tr>
                                <td>${account.name}</td>
                                <td>${account.type.name}</td>
                                <fmt:formatNumber var="formattedBudget" value="${account.budget}" minFractionDigits="2" maxFractionDigits="2"/>
                                <td><c:out value="${formattedBudget} ${account.currency.code}"/></td>
                                <td><a href="${editAccountUrl}" class="btn btn-success w-100">Edit</a></td>
                                <td><a href="${deleteAccountUrl}" class="btn btn-danger w-100">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <notifications:accountLimitNotification/>
</template:page>
