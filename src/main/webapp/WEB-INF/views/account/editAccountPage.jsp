<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/template" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="logoRedirectUrl" value="/"/>
<c:url var="editAccountUrl" value="/account/edit"/>
<template:page pageTitle="Edit account">
    <div class="container">
        <div class="content-container">
            <div class="row justify-content-center">
                <div class="col-11 col-sm-9 col-md-7 col-lg-6 col-xl-5">
                    <div class="mt-4">
                        <div class="text-center">
                            <h3>Edit account</h3>
                        </div>
                        <form:form method="post" action="${editAccountUrl}" modelAttribute="accountForm">
                            <form:hidden path="id"/>
                            <div class="form-group">
                                <form:label path="name">Name</form:label>
                                <form:input path="name" cssClass="form-control" aria-describedby="nameHelp" placeholder="Name"/>
                                <small id="nameHelp" class="form-text text-muted">The account name has to be unique</small>
                                <form:errors path="name" cssClass="color-red"/>
                            </div>
                            <div class="form-group">
                                <form:label path="budget">Budget</form:label>
                                <form:input path="budget" type="number" cssClass="form-control" placeholder="0.0"/>
                                <form:errors path="budget" cssClass="color-red"/>
                            </div>
                            <div class="form-group">
                                <form:label path="currency">Currency</form:label>
                                <form:select path="currency" items="${currencies}" itemLabel="code" cssClass="form-control"/>
                            </div>
                            <div class="form-group">
                                <form:label path="accountType">Account Type</form:label>
                                <form:select path="accountType" items="${accountTypes}" itemLabel="name" cssClass="form-control"/>
                                <form:errors path="accountType" cssClass="color-red"/>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Edit</button>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template:page>
