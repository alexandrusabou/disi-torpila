<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/template" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="settings" tagdir="/WEB-INF/tags/settings"%>

<template:page pageTitle="Settings">
    <div class="container">
        <div class="row text-center">
            <div class="col-12 col-sm-12">
                <c:if test="${not empty successMessage}">
                    <div class="alert alert-success js-timeout w-100">
                        ${successMessage}
                    </div>
                </c:if>
                <c:if test="${not empty errorMessage}">
                    <div class="alert alert-danger js-timeout w-100">
                        ${errorMessage}
                    </div>
                </c:if>
            </div>
        </div>
        <div class="content-container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12 page-title">
                    <h4 class="mb-1">Settings</h4>
                </div>
            </div>
            <div class="border-row-gray mb-4"></div>
            <div class="row justify-content-center">
                <div class="col-xs-12 col-sm-12 col-md-6 mb-3">
                    <settings:defaultCurrencyBox/>
                </div>
            </div>
            <div class="row display-flex">
                <div class="col-xs-12 col-sm-12 col-md-6 mb-3">
                    <settings:currencyBox/>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 mb-3">
                    <settings:accountTypesBox/>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 mb-3">
                    <settings:categoryBox/>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 mb-3">
                    <settings:accountLimitBox/>
                </div>
            </div>
        </div>
    </div>
</template:page>
