<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/template" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:url var="homeUrl" value="/"/>
<template:page pageTitle="Error page" disableHeader="true" disableFooter="true">
    <div class="error-page-bg page-wrap d-flex flex-row align-items-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <span class="display-1 d-block">404</span>
                    <div class="mb-4 lead">The page you are looking for was not found.</div>
                    <a href="${homeUrl}" class="btn btn-primary">Back to Home</a>
                </div>
            </div>
        </div>
    </div>
</template:page>
