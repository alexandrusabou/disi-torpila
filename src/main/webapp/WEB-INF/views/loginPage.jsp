<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/template" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="loginUrl" value="/login"/>
<c:url var="registerUrl" value="/register"/>
<c:url var="logoRedirectUrl" value="/"/>
<template:page pageTitle="Login" disableHeader="true" disableFooter="true">
    <div class="container-fluid login-background">
        <div class="row justify-content-center">
            <div class="col-9 col-sm-7 col-md-5 col-lg-4 col-xl-3">
                <div class="login-container">
                    <div class="text-center mb-3">
                        <a href="${logoRedirectUrl}">
                            <spring:url var="logoImageUrl" value="/resources/images/logo/logo_site.png"/>
                            <img src="${logoImageUrl}" class="login-logo" alt="Logo" title="Homepage"/>
                        </a>
                    </div>
                    <form method="post" action="${loginUrl}">
                        <div class="form-group">
                            <label for="username">Email address</label>
                            <input id="username" name="username" type="text" class="form-control" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input id="password" name="password" type="password" class="form-control" placeholder="Enter password">
                        </div>
                        <div class="form-group">
                            <div class="color-red fs-12">${error}</div>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="remember-me" checked>
                            <label class="form-check-label" for="remember-me">Keep me logged in</label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Login</button>
                        <div class="text-center mt-2">
                            <a href="${registerUrl}" class="fade-underline fs-12">Don't have an account? Sign up</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</template:page>
