<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="disabled" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${empty disabled or not disabled}">
    <c:url var="homeUrl" value="/"/>
    <c:url var="settingsUrl" value="/settings"/>
    <c:url var="logoutUrl" value="/logout"/>
    <spring:url var="logoUrl" value="/resources/images/logo/logo_site.png"/>
    <div class="pm-menu">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-faded mb-3">
                <a class="navbar-brand" href="${homeUrl}"><img class="nav-logo" src="${logoUrl}"/></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Menu">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item mr-3">
                            <a class="nav-link underline-shadow" href="${homeUrl}">
                                <i class="fa fa-home font-header"></i>
                                <span class="header-item">Home</span>
                            </a>
                        </li>
                        <li class="nav-item mr-3">
                            <a class="nav-link underline-shadow" href="${settingsUrl}">
                                <i class="fa fa-cog font-header"></i>
                                <span class="header-item">Settings</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link underline-shadow" href="${logoutUrl}">
                                <i class="fa fa-sign-in font-header"></i>
                                <span class="header-item">Sign out</span>
                            </a>
                        </li>
                    </ul>
                </div>

            </nav>
        </div>
    </div>
</c:if>
