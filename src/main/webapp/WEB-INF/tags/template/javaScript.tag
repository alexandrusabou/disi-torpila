<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url var="resourcesPath" value="/resources"/>

<script type="text/javascript" src="${resourcesPath}/js/lib/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="${resourcesPath}/js/lib/bootstrap.min.js"></script>
<script type="text/javascript" src="${resourcesPath}/js/main.js"></script>