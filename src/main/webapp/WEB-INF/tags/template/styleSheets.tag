<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url var="resourcesPath" value="/resources"/>

<link rel="stylesheet" type="text/css" href="${resourcesPath}/css/lib/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${resourcesPath}/css/lib/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="${resourcesPath}/css/main.css"/>