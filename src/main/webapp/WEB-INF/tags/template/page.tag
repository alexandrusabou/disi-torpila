<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" type="java.lang.String" %>
<%@ attribute name="disableHeader" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disableFooter" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/common" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/template" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${not empty pageTitle ? pageTitle : ""}</title>

        <%-- CSS files --%>
        <template:styleSheets/>
    </head>
    <body>
        <main class="login-background">
            <%-- Common header for all pages --%>
            <common:header disabled="${disableHeader}"/>

            <%-- Inject page body --%>
            <jsp:doBody/>

            <%-- Common footer for all pages --%>
            <common:footer disabled="${disableFooter}"/>
        </main>
        <%-- JavaScript files --%>
        <template:javaScript/>
    </body>
</html>