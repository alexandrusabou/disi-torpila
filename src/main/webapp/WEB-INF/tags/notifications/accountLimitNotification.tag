<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="modal fade" id="notificationModel" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel" aria-hidden="true" data-accounts="${notificationAccounts}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notificationModalLabel">Account limit notification</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mb-2">The following accounts are under the limit:</div>
                <table class="table table-bordered box-shadow">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Type</th>
                        <th scope="col">Budget</th>
                        <th scope="col">Limit</th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="account" items="${notificationAccounts}">
                            <tr>
                                <td>${account.name}</td>
                                <td>${account.type.name}</td>
                                <fmt:formatNumber var="formattedBudget" value="${account.budget}" minFractionDigits="2" maxFractionDigits="2"/>
                                <td><c:out value="${formattedBudget} ${account.currency.code}"/></td>
                                <fmt:formatNumber var="formattedLimit" value="${account.notificationLimit}" minFractionDigits="2" maxFractionDigits="2"/>
                                <td class="color-remove"><c:out value="${formattedLimit} ${account.currency.code}"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>