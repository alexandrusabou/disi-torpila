<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="addCategoryUrl" value="/category/add"/>
<c:url var="editCategoryUrl" value="/category/edit"/>
<c:url var="deleteCategoryUrl" value="/category/delete"/>
<div class="card box-shadow h-100">
    <h6 class="card-header">Spending category</h6>
    <div class="card-body">
        <form:form cssClass="js-category-form" method="post" action="${addCategoryUrl}" data-values="${categories}" modelAttribute="categoryForm">
            <div class="row">
                <div class="col-7 col-sm-7 col-md-7 col-lg-8">
                    <div class="form-group">
                        <form:select path="category" items="${categories}" cssClass="form-control" itemValue="id" itemLabel="name"/>
                    </div>
                </div>
                <div class="col-5 col-sm-5 col-md-5 col-lg-4 px-0">
                    <a title="Edit" href="#editCategoryContent" data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="editContent">
                        <i class="fa fa-cog fa-2x color-edit pr-2"></i>
                    </a>
                    <a title="Delete" href="#deleteCategoryContent" data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="deleteCategoryContent">
                        <i class="fa fa-times fa-2x color-remove pr-2"></i>
                    </a>
                    <a title="Add" href="#addCategoryContent" data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="addCategoryContent">
                        <i class="fa fa-plus fa-2x color-add"></i>
                    </a>
                </div>
            </div>
            <c:choose>
                <c:when test="${empty categories}">
                    <c:set var="actionDisabled" value="disabled"/>
                </c:when>
                <c:otherwise>
                    <c:set var="actionDisabled" value=""/>
                </c:otherwise>
            </c:choose>
            <div id="categoryGroup">
                <c:set var="editErrors"><form:errors path="editName"/></c:set>
                <div id="editCategoryContent" class="collapse ${not empty editErrors ? "show" : ""}" data-parent="#categoryGroup">
                    <div class="card card-body box-shadow-no-hover">

                        <div class="form-group card-text">
                            <form:label path="editName">Name</form:label>
                            <form:input path="editName" type="text" cssClass="form-control" placeholder="Name" value="${categories[0].name}" disabled="${not empty actionDisabled}"/>
                            <form:errors path="editName" cssClass="color-red"/>
                        </div>
                        <button type="submit" class="btn btn-primary" formaction="${editCategoryUrl}" ${actionDisabled}>Edit</button>
                    </div>
                </div>
                <c:set var="addErrors"><form:errors path="addName"/></c:set>
                <div id="addCategoryContent" class="collapse ${not empty addErrors ? "show" : ""}" data-parent="#categoryGroup">
                    <div class="card card-body card-text box-shadow-no-hover">
                        <div class="form-group">
                            <form:label path="addName">Name</form:label>
                            <form:input path="addName" type="text" cssClass="form-control" placeholder="Name"/>
                            <form:errors path="addName" cssClass="color-red"/>
                        </div>
                        <button type="submit" class="btn btn-success">Add</button>
                    </div>
                </div>
                <div id="deleteCategoryContent" class="collapse" data-parent="#categoryGroup">
                    <div class="card card-body card-text box-shadow-no-hover">
                        <div class="card-title text-center">Are you sure?</div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-danger" formaction="${deleteCategoryUrl}" ${actionDisabled}>Delete</button>
                            <a href="#" class="btn btn-secondary js-cancel-delete">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form:form>
    </div>
</div>