<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="editAccountLimitUrl" value="/account/limit/update"/>
<div class="card box-shadow h-100">
    <h6 class="card-header">Account limit</h6>
    <div class="card-body">
        <form:form cssClass="js-accountLimit-form" method="post" action="${editAccountLimitUrl}" data-values="${accounts}" modelAttribute="accountLimitForm">
            <div class="row">
                <div class="col-9 col-sm-9 col-md-9 col-lg-10">
                    <div class="form-group">
                        <select id="account" name="account" class="form-control">
                            <c:forEach var="account" items="${accounts}">
                                <option value="${account.id}" label="${account.name} - ${account.type.name}"></option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-3 col-sm-3 col-md-3 col-lg-2 px-0">
                    <a title="Edit" href="#editAccountLimitContent" data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="editContent">
                        <i class="fa fa-cog fa-2x color-edit pr-2"></i>
                    </a>
                </div>
            </div>
            <c:choose>
                <c:when test="${empty accounts}">
                    <c:set var="actionDisabled" value="disabled"/>
                </c:when>
                <c:otherwise>
                    <c:set var="actionDisabled" value=""/>
                </c:otherwise>
            </c:choose>
            <div id="accountLimitGroup">
                <c:set var="editErrors"><form:errors path="limit"/></c:set>
                <div id="editAccountLimitContent" class="collapse ${not empty editErrors ? "show" : ""}" data-parent="#accountLimitGroup">
                    <div class="card card-body box-shadow-no-hover">
                        <div class="form-group card-text">
                            <form:label path="limit">Limit</form:label>
                            <form:input path="limit" type="number" cssClass="form-control" placeholder="0.0" value="${accounts[0].notificationLimit}" disabled="${not empty actionDisabled}"/>
                            <form:errors path="limit" cssClass="color-red"/>
                        </div>
                        <button type="submit" class="btn btn-primary" formaction="${editAccountLimitUrl}" ${actionDisabled}>Edit</button>
                    </div>
                </div>
            </div>
        </form:form>
    </div>
</div>