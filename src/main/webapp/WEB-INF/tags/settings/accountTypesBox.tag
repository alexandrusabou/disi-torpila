<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="addAccountTypeUrl" value="/accountType/add"/>
<c:url var="editAccountTypeUrl" value="/accountType/edit"/>
<c:url var="deleteAccountTypeUrl" value="/accountType/delete"/>
<div class="card box-shadow h-100">
    <h6 class="card-header">Account Type</h6>
    <div class="card-body">
        <form:form cssClass="js-accountType-form" method="post" action="${addAccountTypeUrl}" data-values="${accountTypes}" modelAttribute="accountTypeForm">
            <div class="row">
                <div class="col-7 col-sm-7 col-md-7 col-lg-8">
                    <div class="form-group">
                        <form:select path="accountType" items="${accountTypes}" cssClass="form-control" itemValue="id" itemLabel="name"/>
                    </div>
                </div>
                <div class="col-5 col-sm-5 col-md-5 col-lg-4 px-0">
                    <a title="Edit" href="#editAccountTypeContent" data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="editContent">
                        <i class="fa fa-cog fa-2x color-edit pr-2"></i>
                    </a>
                    <a title="Delete" href="#deleteAccountTypeContent" data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="deleteAccountTypeContent">
                        <i class="fa fa-times fa-2x color-remove pr-2"></i>
                    </a>
                    <a title="Add" href="#addAccountTypeContent" data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="addAccountTypeContent">
                        <i class="fa fa-plus fa-2x color-add"></i>
                    </a>
                </div>
            </div>
            <c:choose>
                <c:when test="${empty accountTypes}">
                    <c:set var="actionDisabled" value="disabled"/>
                </c:when>
                <c:otherwise>
                    <c:set var="actionDisabled" value=""/>
                </c:otherwise>
            </c:choose>
            <div id="accountTypeGroup">
                <c:set var="editErrors"><form:errors path="editName"/></c:set>
                <div id="editAccountTypeContent" class="collapse ${not empty editErrors ? "show" : ""}" data-parent="#accountTypeGroup">
                    <div class="card card-body box-shadow-no-hover">
                        <div class="form-group card-text">
                            <form:label path="editName">Name</form:label>
                            <form:input path="editName" type="text" cssClass="form-control" placeholder="Name" value="${accountTypes[0].name}" disabled="${not empty actionDisabled}"/>
                            <form:errors path="editName" cssClass="color-red"/>
                        </div>
                        <button type="submit" class="btn btn-primary" formaction="${editAccountTypeUrl}" ${actionDisabled}>Edit</button>
                    </div>
                </div>
                <c:set var="addErrors"><form:errors path="addName"/></c:set>
                <div id="addAccountTypeContent" class="collapse ${not empty addErrors ? "show" : ""}" data-parent="#accountTypeGroup">
                    <div class="card card-body card-text box-shadow-no-hover">
                        <div class="form-group">
                            <form:label path="addName">Name</form:label>
                            <form:input path="addName" type="text" cssClass="form-control" placeholder="Name"/>
                            <form:errors path="addName" cssClass="color-red"/>
                        </div>
                        <button type="submit" class="btn btn-success">Add</button>
                    </div>
                </div>
                <div id="deleteAccountTypeContent" class="collapse" data-parent="#accountTypeGroup">
                    <div class="card card-body card-text box-shadow-no-hover">
                        <div class="card-title text-center">Are you sure?</div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-danger" formaction="${deleteAccountTypeUrl}" ${actionDisabled}>Delete</button>
                            <a href="#" class="btn btn-secondary js-cancel-delete">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form:form>
    </div>
</div>