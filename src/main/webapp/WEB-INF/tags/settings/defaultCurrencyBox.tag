<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="editDefaultCurrencyUrl" value="/user/currency/update"/>
<div class="card box-shadow h-100">
    <h6 class="card-header">Default currency</h6>
    <div class="card-body">
        <form:form cssClass="js-defaultCurrency-form" method="post" action="${editDefaultCurrencyUrl}" data-values="${currencies}" data-default="${defaultCurrencyForm.defaultCurrency}" modelAttribute="defaultCurrencyForm">
            <div class="row">
                <div class="col-8 col-sm-8 col-md-8 col-lg-9">
                    <form:hidden path="defaultCurrency"/>
                    <div class="form-group">
                        <select id="currency" name="currency" class="form-control">
                            <c:forEach var="currency" items="${currencies}">
                                <c:choose>
                                    <c:when test="${currency.id eq defaultCurrencyForm.defaultCurrency.id}">
                                        <option value="${currency.id}" label="${currency.code}" selected></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${currency.id}" label="${currency.code}"></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-4 col-sm-4 col-md-4 col-lg-3 px-0">
                    <button type="button" title="Refresh" class="mouse-pointer js-refresh-def-currency">
                        <i class="fa fa-refresh fa-2x color-edit pr-2"></i>
                    </button>
                    <button type="submit" title="Set" class="mouse-pointer">
                        <i class="fa fa-check fa-2x color-add pr-2"></i>
                    </button>
                </div>
            </div>
        </form:form>
    </div>
</div>