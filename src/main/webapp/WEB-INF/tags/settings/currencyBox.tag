<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="addCurrencyUrl" value="/currency/add"/>
<c:url var="editCurrencyUrl" value="/currency/edit"/>
<c:url var="deleteCurrencyUrl" value="/currency/delete"/>
<div class="card box-shadow h-100">
    <h6 class="card-header">Currency</h6>
    <div class="card-body">
        <form:form cssClass="js-currency-form" method="post" action="${addCurrencyUrl}" data-values="${currencies}" modelAttribute="currencyForm">
            <div class="row">
                <div class="col-7 col-sm-7 col-md-7 col-lg-8">
                    <div class="form-group">
                        <form:select path="currency" items="${currencies}" itemLabel="code" cssClass="form-control"/>
                    </div>
                </div>
                <div class="col-5 col-sm-5 col-md-5 col-lg-4 px-0">
                    <a title="Edit" href="#editCurrencyContent"  data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="editContent">
                        <i class="fa fa-cog fa-2x color-edit pr-2"></i>
                    </a>
                    <a title="Delete" href="#deleteCurrencyContent" data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="deleteCurrencyContent">
                        <i class="fa fa-times fa-2x color-remove pr-2"></i>
                    </a>
                    <a title="Add" href="#addCurrencyContent" data-toggle="collapse"
                       role="button" aria-expanded="false" aria-controls="addCurrencyContent">
                        <i class="fa fa-plus fa-2x color-add"></i>
                    </a>
                </div>
            </div>
            <c:choose>
                <c:when test="${empty currencies}">
                    <c:set var="actionDisabled" value="disabled"/>
                </c:when>
                <c:otherwise>
                    <c:set var="actionDisabled" value=""/>
                </c:otherwise>
            </c:choose>
            <div id="currencyGroup">
                <c:set var="editErrors"><form:errors path="editName"/></c:set>
                <div id="editCurrencyContent" class='collapse ${not empty editErrors ? "show" : ""}' data-parent="#currencyGroup">
                    <div class="card card-body box-shadow-no-hover">
                        <div class="form-group card-text">
                            <form:label path="editName">Name</form:label>
                            <form:input path="editName" type="text" cssClass="form-control" placeholder="Name" value="${currencies[0].name}" disabled="${not empty actionDisabled}"/>
                            <form:errors path="editName" cssClass="color-red"/>
                        </div>
                        <button type="submit" class="btn btn-primary" formaction="${editCurrencyUrl}" ${actionDisabled}>Edit</button>
                    </div>
                </div>
                <c:set var="addErrors"><form:errors path="addName"/></c:set>
                <c:if test="${empty addErrors}">
                    <c:set var="addErrors"><form:errors path="code"/></c:set>
                </c:if>
                <div id="addCurrencyContent" class='collapse ${not empty addErrors ? "show" : ""}' data-parent="#currencyGroup">
                    <div class="card card-body card-text box-shadow-no-hover">
                        <div class="form-group">
                            <form:label path="code">Code</form:label>
                            <form:input path="code" type="text" cssClass="form-control" placeholder="Code"/>
                            <form:errors path="code" cssClass="color-red"/>
                        </div>
                        <div class="form-group">
                            <form:label path="addName">Name</form:label>
                            <form:input path="addName" type="text" cssClass="form-control" placeholder="Name"/>
                            <form:errors path="addName" cssClass="color-red"/>
                        </div>
                        <button type="submit" class="btn btn-success">Add</button>
                    </div>
                </div>
                <div id="deleteCurrencyContent" class="collapse" data-parent="#currencyGroup">
                    <div class="card card-body card-text box-shadow-no-hover">
                        <div class="card-title text-center">Are you sure?</div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-danger" formaction="${deleteCurrencyUrl}" ${actionDisabled}>Delete</button>
                            <a href="#" class="btn btn-secondary js-cancel-delete">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form:form>
    </div>
</div>